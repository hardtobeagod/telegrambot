
"""
Simple bot for saving voice_notes
"""

from telegram.ext import Updater, MessageHandler, Filters
import logging
import os


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.


def save_voice_note(bot, update):
    """Save a voice note in filesystem"""
    file_id = update.message.voice.file_id
    user_id = update.message.from_user.id
    new_file = bot.get_file(file_id)
    user_exists = os.path.exists('bot/' + str(user_id))
    if not user_exists:
        os.makedirs('bot/' + str(user_id))
    new_file.download('bot/' + str(user_id) + '/voice' + str(update.message.date) + '.ogg')


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    """Start the bot."""
    # Create the EventHandler and pass it your bot's token.
    updater = Updater("618040679:AAGDmXA9sAvq19nbHg2FsYlmAA5hz1N8w2c")

    # Get the dispatcher to register handlers
    dp = updater.dispatcher
    dp.add_handler(MessageHandler(Filters.voice, save_voice_note))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
